Name:           atf
Version:        0.21
Release:        3
License:        BSD
Summary:        Testing Framework for Automated
URL:            https://github.com/jmmv/atf/
Source0:        https://github.com/jmmv/atf/archive/atf-0.21.tar.gz

Patch0:         Replace-auto_ptr-with-unique_ptr.patch

BuildRequires:  gcc-c++ autoconf libtool

%description
No main package is built here. Because it is not used.

The Automated Test Framework (ATF) is a collection of libraries used to implement test programs
in multiple languages. Currently, ATF provides C, C ++ and POSIX Shell bindings for performing
tests. These bindings provide a similar feature set, and any test program written with them exposes
a consistent user interface.
ATF-based test programs rely on separate runtime engines to execute them. The runtime engine is
responsible for isolating the test program from the rest of the system to ensure that its results
are deterministic and do not affect the running system. The runtime engine is also responsible for
collecting the results of all tests and writing reports. The currently selected runtime is Kyua.

%package -n libatf-c
Summary: Testing Framework for Automated - C bindings

%description -n libatf-c
This package contains a runtime library to run tests of the ATF C bindings.

%package -n libatf-c-devel
Summary:  ATesting Framework for Automated - C bindings (headers)
Requires: libatf-c = %{version}-%{release}

%description -n libatf-c-devel
This package contains ATF C bindings headers and libraries.

%package -n libatf-c++
Summary: Testing Framework for Automated - C++ bindings

%description -n libatf-c++
This package contains a runtime library to run tests of the ATF C++ bindings.

%package -n libatf-c++-devel
Summary:  Testing Framework for Automated - C++ bindings (headers)
Requires: libatf-c = %{version}-%{release} libatf-c-devel = %{version}-%{release}
Requires: libatf-c++ = %{version}-%{release}
%description -n libatf-c++-devel
This package contains ATF C++ bindings headers and libraries.

%package -n libatf-sh
Summary:  Testing Framework for Automated - POSIX shell bindings
Requires: libatf-c++ = %{version}-%{release}

%description -n libatf-sh
This package contains a runtime library to run tests of the ATF POSIX shell bindings.

%package -n libatf-sh-devel
Summary:  Testing Framework for Automated - POSIX shell bindings (headers)
Requires: libatf-sh = %{version}-%{release}

%description -n libatf-sh-devel
This package contains POSIX shell bindings headers and libraries.

%package tests
Summary:  Testing Framework for Automated - Test suite
Requires: libatf-c = %{version}-%{release}  libatf-c++ = %{version}-%{release}
Requires: libatf-sh = %{version}-%{release} libatf-c-devel = %{version}-%{release}
Requires: libatf-c++-devel = %{version}-%{release} libatf-sh-devel = %{version}-%{release}

%description tests
This package contains all ATF component installation runtime tests, including tests for C, C++
and POSIX Shell libraries and runtime tools.
For more details on how to run the installed tests, see the README file in the documentation
directory.

%package help
Summary: Help documents for atf

%description help
Man pages and other related help documents for atf.

%prep
%autosetup -n atf-%{name}-%{version} -p1

%build
autoreconf -i -s
%configure INSTALL="/usr/bin/install -p" --disable-static
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%make_build pkgtestsdir=%{_libexecdir}/atf/tests testsdir=%{_libexecdir}/atf/tests

%install
%make_install doc_DATA= pkgtestsdir=%{_libexecdir}/atf/tests testsdir=%{_pkgtestsdir}
%delete_la

%check
make check

%post
/sbin/ldconfig
%postun
/sbin/ldconfig

%files -n libatf-c
%{_libdir}/libatf-c.{so.1,so.1.0.0}
%{_datadir}/man/man1/atf-test-program.1.gz
%{_datadir}/man/man4/atf-test-case.4.gz

%files -n libatf-c-devel
%{_includedir}/{atf-c.h,atf-c}
%{_datadir}/aclocal/{atf-c,atf-common}.m4
%{_libdir}/{libatf-c.so,pkgconfig/atf-c.pc}

%files -n libatf-c++
%{_libdir}/libatf-c++.{so.2,so.2.0.0}

%files -n libatf-c++-devel
%{_datadir}/aclocal/atf-c++.m4
%{_includedir}/{atf-c++.hpp,atf-c++}
%{_libdir}/{libatf-c++.so,pkgconfig/atf-c++.pc}

%files -n libatf-sh
%{_datadir}/atf
%{_bindir}/atf-sh
%{_libexecdir}/atf-check

%files -n libatf-sh-devel
%{_datadir}/aclocal/atf-sh.m4
%{_libdir}/pkgconfig/atf-sh.pc

%files tests
%doc README
%{_libexecdir}/atf/tests

%files help
%{_mandir}/man1/*
%{_mandir}/man3/*
%{_mandir}/man7/*

%changelog
* Sun Sep 29 2024 xu_ping <707078654@qq.com> - 0.21-3
- Replace auto_ptr with unique_ptr

* Thu Dec 31 2020 Ge Wang <wangge20@huawei.com> - 0.21-2
- Modify Source url and remove redundancy source file

* Thu Jun  4 2020 sunguoshuai <sunguoshuai@huawei.com> 0.21-1
- Package update to 0.21-1

* Tue Dec 31 2019 duyeyu <duyeyu@huawei.com> - 0.20-13
- Package init
